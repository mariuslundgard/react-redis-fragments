# react-redis-fragments

This project consists of 3 servers:

* `api` renders React fragments
* `client` renders React framework
* `redis` for caching

## Create a docker-machine

This assumes you have VirtualBox, Docker and docker-compose installed.

For this prototype app, the machine **must** be named `fragment`:

```
docker-machine create fragment -d virtualbox
eval "$(docker-machine env fragment)"
```

## Setup Redis in docker

```
git clone https://mariuslundgard@bitbucket.org/mariuslundgard/react-redis-fragments.git
cd react-redis-fragments
docker-compose up
```

## Start the api and client servers

```
cd api
npm install
npm start
```

```
cd client
npm install
npm start
```

The `client` project also has a build script:

```
cd client
npm run build
# Or: npm run watch
```

Visit http://localhost:3000/.

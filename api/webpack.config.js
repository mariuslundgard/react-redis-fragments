var path = require('path')
var webpack = require('webpack')

module.exports = {
  entry: {
    masthead: './app/fragments/masthead/bootstrap.js',
    footer: './app/fragments/footer/bootstrap.js'
  },
  output: {
    path: path.resolve(__dirname, 'static'),
    filename: '[name]/bootstrap.js',
    library: '[name]',
    libraryTarget: 'umd'
  },
  module: {
    loaders: [
      { test: /\.js$/, loader: 'babel?optional[]=runtime&stage=0', exclude: /(node_modules|static|webpack)/ }
    ]
  },
  resolve: {
    root: [path.resolve(__dirname, 'app')]
  },
  plugins: [
    new webpack.DefinePlugin({
      __CLIENT__: true,
      __SERVER__: false,
      __DEVELOPMENT__: false
    }),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false
      },
      output: {
        comments: false
      }
    })
  ]
}

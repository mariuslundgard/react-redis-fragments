import pkg from '../../package.json'
import bunyan from 'bunyan'

export default bunyan.createLogger({
  name: pkg.name
})

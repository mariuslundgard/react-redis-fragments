/* global __CLIENT__ */

// NOTE: The following is the hackiest part of this POC,
// done in order to only bundle React once.
// See `webpack.config.js` for the global `__CLIENT__` definition.

export default (
  typeof __CLIENT__ !== 'undefined' && __CLIENT__ ? window.React : require('react')
)

import path from 'path'
import koa from 'koa'
import serve from 'koa-static'
import router from 'router'
import logger from 'logger'

const app = koa()
const port = process.env.PORT || 4000

app.use(serve(path.resolve(__dirname, '../static')))
app.use(function * (next) {
  logger.info(this.method, this.url)
  yield next
})
app.use(router.routes())
app.use(router.allowedMethods())

app.listen(port)
logger.info('the application is listening at localhost:' + port)

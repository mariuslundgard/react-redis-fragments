import Router from 'koa-router'
import mastheadHandler from 'fragments/masthead/handler'
import footerHandler from 'fragments/footer/handler'

const router = Router()

router.get('/v1/fragment/masthead', mastheadHandler)
router.get('/v1/fragment/footer', footerHandler)

export default router

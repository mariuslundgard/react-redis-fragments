import React from 'react'
import FooterFragment from 'fragments/footer/component'

function render (props) {
  return new Promise((resolve, reject) => {
    resolve(React.renderToString(<FooterFragment {...props} />))
  })
}

export default function * () {
  this.body = yield render(this.query)
}

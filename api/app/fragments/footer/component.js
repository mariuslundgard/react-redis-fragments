import React from 'react-master'

class FooterFragment extends React.Component {
  static displayName = 'FooterFragment'

  static propTypes = {
    domain: React.PropTypes.string.isRequired
  }

  render () {
    const propsJSON = decodeURIComponent(JSON.stringify(this.props))

    return (
      <div data-props={propsJSON} className='footer'>
        <hr />
        <div>
          Kolofon for NRK <strong>{this.props.domain}</strong>
        </div>
      </div>
    )
  }
}

export default FooterFragment

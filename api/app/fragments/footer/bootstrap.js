import React from 'react-master'
import FooterFragment from 'fragments/footer/component'

const name = 'footer'
const mountPoints = Array.prototype.slice.call(document.querySelectorAll(
  `.fragment-container.${name}-container.js-initialize`
))

mountPoints.forEach(container => {
  const props = JSON.parse(decodeURIComponent(container.firstChild.dataset.props))
  container.className = container.className.replace(' js-initialize', '')
  React.render(<FooterFragment {...props} />, container)
})

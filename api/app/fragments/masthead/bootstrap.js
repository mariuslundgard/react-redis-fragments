import React from 'react-master'
import MastheadFragment from 'fragments/masthead/component'

const name = 'masthead'
const mountPoints = Array.prototype.slice.call(document.querySelectorAll(
  `.fragment-container.${name}-container.js-initialize`
))

mountPoints.forEach(container => {
  const props = JSON.parse(decodeURIComponent(container.firstChild.dataset.props))
  container.className = container.className.replace(' js-initialize', '')
  React.render(<MastheadFragment {...props} />, container)
})

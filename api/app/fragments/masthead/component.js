import React from 'react-master'

const links = [
  ['/nyheter', 'Nyheter'],
  ['/sport', 'Sport'],
  ['/tv', 'TV'],
  ['/radio', 'Radio'],
  ['/distrikt', 'Distrikt']
]

class MastheadFragment extends React.Component {
  static displayName = 'MastheadFragment'

  static propTypes = {
    path: React.PropTypes.string.isRequired
  }

  renderLinks () {
    return links.map(([href, label], ix) => {
      const linkProps = {key: `link-${ix}`}

      if (this.props.path.indexOf(href) === 0) {
        linkProps.className = 'is-active'
      }

      return (
        <li {...linkProps}><a href={href}>{label}</a></li>
      )
    })
  }

  render () {
    const inlineStyles = {
      background: '#222',
      color: '#fff'
    }

    const propsJSON = JSON.stringify(this.props)

    return (
      <header data-props={propsJSON} className='masthead' style={inlineStyles} role='banner'>
        <a href='/'>NRK</a>

        <nav>
          <ol>
            {this.renderLinks()}
          </ol>
        </nav>
      </header>
    )
  }
}

export default MastheadFragment

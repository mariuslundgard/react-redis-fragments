import React from 'react'
import MastheadFragment from 'fragments/masthead/component'

function render (props) {
  return new Promise((resolve, reject) => {
    resolve(React.renderToString(<MastheadFragment {...props} />))
  })
}

export default function * () {
  this.body = yield render(this.query)
}

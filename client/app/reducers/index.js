import {combineReducers} from 'redux'

const initialState = {}

function testReducer (state = initialState, action) {
  return state
}

export default combineReducers({
  test: testReducer
})

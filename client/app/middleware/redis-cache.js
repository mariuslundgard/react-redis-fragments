import redis from 'redis'
import config from 'config'

export default function * redisCache (next) {
  this.redis = redis.createClient(config.get('redis.port'), config.get('redis.host'), {})
  yield next
}

/*
 * TODO
 * - Implement a fast memory cache layer in front of HTTP requests
 * - Consider what sort of DSL works well, if pure HTML5 is best, etc.
 */

import http from 'http'
import crypto from 'crypto'
import querystring from 'querystring'

const API_BASE_URL = 'http://localhost:4000/v1/fragment/'

function fetchCachedFragment (redisClient, cacheKey) {
  return new Promise((resolve, reject) => {
    redisClient.get(cacheKey, (err, reply) => {
      if (err) {
        reject(err)
      } else {
        resolve(reply)
      }
    })
  })
}

function fetchFragment (redisClient, {input, name, data}) {
  return new Promise((resolve, reject) => {
    try {
      data = JSON.parse(data)
    } catch (err) {
      reject(err)
      return
    }

    const cacheSecret = 'secret'
    const cacheKey = crypto.createHmac('sha1', cacheSecret).update(input).digest('hex')

    fetchCachedFragment(redisClient, cacheKey).then(output => {
      if (output) {
        resolve({input, name, data, output})
        return
      }

      const dataQs = querystring.stringify(data)

      http.get(API_BASE_URL + name + (dataQs.length ? `?${dataQs}` : ''), function (res) {
        let output = ''

        res.on('data', function (text) {
          output += text
        })

        res.on('end', function () {
          redisClient.set(cacheKey, output)
          redisClient.expire(cacheKey, data.expire || 10)
          resolve({input, name, data, output})
        })
      }).on('error', reject)
    }).catch(reject)
  })
}

function fetchFragments (redisClient, optsArray) {
  return Promise.all(optsArray.map(opts => {
    return fetchFragment(redisClient, opts)
  }))
}

export default function * fragmentLoader (next) {
  try {
    yield next

    // Define fragment pattern
    const pattern = /\[\[([a-z]+)\:(.+?)\]\]/g

    // Find matches
    const matches = []
    let match = pattern.exec(this.body)
    while (match) {
      matches.push(match)
      match = pattern.exec(this.body)
    }

    // Fetch HTML output from the API server
    const fragments = yield fetchFragments(this.redis, matches.map(m => {
      return {
        input: m[0],
        name: m[1],
        data: m[2]
      }
    }))

    // Apply the fragments to the body text
    fragments.forEach(fragment => {
      this.body = this.body.replace(fragment.input, fragment.output)
    })
  } catch (err) {
    this.body = err.message
  }
}

import React from 'react'
import createLocation from 'history/lib/createLocation'
import createHistory from 'history/lib/createBrowserHistory'
import configureStore from 'store/configure-store'
import universalRouter from 'router/universal'

window.React = React

const location = createLocation(document.location.pathname, document.location.search)
const history = createHistory()
const store = configureStore({})

universalRouter(location, history, store).then(component => {
  React.render(component, document.querySelector('.root-container'))
})

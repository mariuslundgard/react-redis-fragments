import React from 'react'
import Fragment from 'components/fragment'

class RadioPage extends React.Component {
  static propTypes = {
    location: React.PropTypes.object.isRequired
  }

  static fetchData = () => {

  }

  render () {
    const {location} = this.props

    return (
      <div className='radio page'>
        <Fragment
          name='masthead'
          path={location.pathname} />

        <div className='page--content'>
          <h1>Radio</h1>
        </div>

        <Fragment
          name='footer'
          domain='radio' />
      </div>
    )
  }
}

export default RadioPage

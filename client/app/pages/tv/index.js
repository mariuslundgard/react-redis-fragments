import React from 'react'
import Fragment from 'components/fragment'

class TVPage extends React.Component {
  static propTypes = {
    location: React.PropTypes.object.isRequired
  }

  static fetchData = () => {

  }

  render () {
    const {location} = this.props

    return (
      <div className='tv page'>
        <Fragment
          name='masthead'
          path={location.pathname} />

        <div className='page--content'>
          <h1>TV</h1>
        </div>

        <Fragment
          name='footer'
          domain='tv' />
      </div>
    )
  }
}

export default TVPage

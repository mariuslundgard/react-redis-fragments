import React from 'react'
import Fragment from 'components/fragment'

class FrontPage extends React.Component {
  static propTypes = {
    location: React.PropTypes.object.isRequired
  }

  static fetchData = () => {

  }

  render () {
    const {location} = this.props

    return (
      <div className='front page'>
        <Fragment
          name='masthead'
          path={location.pathname} />

        <div className='page--content'>
          <h1>Front</h1>
        </div>

        <Fragment
          name='footer'
          domain='front' />
      </div>
    )
  }
}

export default FrontPage

import React from 'react'
import Fragment from 'components/fragment'

class NewsPage extends React.Component {
  static propTypes = {
    location: React.PropTypes.object.isRequired
  }

  static fetchData = () => {

  }

  render () {
    const {location} = this.props

    return (
      <div className='news page'>
        <Fragment
          name='masthead'
          path={location.pathname} />

        <div className='page--content'>
          <h1>Nyheter</h1>
        </div>

        <Fragment
          name='footer'
          domain='news' />
      </div>
    )
  }
}

export default NewsPage

import React from 'react'
import Fragment from 'components/fragment'

class LocalPage extends React.Component {
  static propTypes = {
    location: React.PropTypes.object.isRequired
  }

  static fetchData = () => {

  }

  render () {
    const {location} = this.props

    return (
      <div className='local page'>
        <Fragment
          name='masthead'
          path={location.pathname} />

        <div className='page--content'>
          <h1>Distrikt</h1>
        </div>

        <Fragment
          name='footer'
          domain='local' />
      </div>
    )
  }
}

export default LocalPage

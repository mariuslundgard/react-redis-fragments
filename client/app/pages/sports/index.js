import React from 'react'
import Fragment from 'components/fragment'

class SportsPage extends React.Component {
  static propTypes = {
    location: React.PropTypes.object.isRequired
  }

  static fetchData = () => {

  }

  render () {
    const {location} = this.props

    return (
      <div className='sports page'>
        <Fragment
          name='masthead'
          path={location.pathname} />

        <div className='page--content'>
          <h1>Sport</h1>
        </div>

        <Fragment
          name='footer'
          domain='sports' />
      </div>
    )
  }
}

export default SportsPage

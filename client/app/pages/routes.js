import React from 'react'
import {Route} from 'react-router'
import Root from 'components/root'

// Page components
import FrontPage from 'pages/front'
import NewsPage from 'pages/news'
import SportsPage from 'pages/sports'
import TVPage from 'pages/tv'
import RadioPage from 'pages/radio'
import LocalPage from 'pages/local'

export function createRoutes () {
  const routes = (
    <Route component={Root}>
      <Route path='/' component={FrontPage} />
      <Route path='/nyheter' component={NewsPage} />
      <Route path='/sport' component={SportsPage} />
      <Route path='/tv' component={TVPage} />
      <Route path='/radio' component={RadioPage} />
      <Route path='/distrikt' component={LocalPage} />
    </Route>
  )

  return routes
}

var path = require('path')
var convict = require('convict')

// Define a schema
var conf = convict({
  env: {
    doc: 'The applicaton environment.',
    format: ['production', 'development', 'test', 'local'],
    default: 'development',
    env: 'NODE_ENV'
  },
  ip: {
    doc: 'The IP address to bind.',
    format: 'ipaddress',
    default: '127.0.0.1',
    env: 'IP_ADDRESS'
  },
  port: {
    doc: 'The port to bind.',
    format: 'port',
    default: 8080,
    env: 'PORT'
  },
  redis: {
    host: {
      doc: 'The Redis host name.',
      format: String,
      default: '127.0.0.1',
      env: 'REDIS_HOST'
    },
    port: {
      doc: 'The Redis port.',
      format: 'port',
      default: 6379,
      env: 'REDIS_PORT'
    }
  }
})

// Load environment dependent configuration
var env = conf.get('env')
conf.loadFile(path.resolve(__dirname, env + '.json'))

// Perform validation
conf.validate({strict: true})

module.exports = conf

import React from 'react'
import Router from 'koa-router'
import universalRouter from 'router/universal'
import createLocation from 'history/lib/createLocation'
import configureStore from 'store/configure-store'

const router = Router()

function render (url) {
  return new Promise((resolve, reject) => {
    const location = createLocation(url)
    const store = configureStore()

    universalRouter(location, null, store).then(component => {
      resolve(React.renderToString(component))
    })
  })
}

router.get('*', function * () {
  try {
    const buf = []

    buf.push(
      `<!DOCTYPE html>`,
      `<html lang="no">`,
      `<head>`,
      `<title>NRK</title>`,
      `<style>`,
      `* {margin: 0; padding: 0; box-sizing: border-box; font-size: inherit; font-weight: inherit;}`,
      `body {font-family: 'Helvetica Neue'; line-height: 1.25; -webkit-font-smoothing: antialiased;}`,
      `a {color: inherit; text-decoration: none;}`,
      `.masthead {max-width: 1180px; margin: 0 auto;}`,
      `.masthead > a {display: inline-block; padding: 15px;}`,
      `.masthead {display: flex; width: 100%;}`,
      `.masthead ol {display: inline-flex; list-style-type: none}`,
      `.masthead li {display: block; position: relative;}`,
      `.masthead li a {display: block; padding: 15px;}`,
      `.masthead li.is-active a {background: #2c2c2c;}`,
      `.masthead li.is-active:after {position: absolute; content: ''; display: block; left: 0; right: 0; bottom: -3px; height: 3px; background: #09f; pointer-events: none;}`,
      `.page--content {max-width: 1180px; padding: 15px; margin: 0 auto;}`,
      `.page--content h1 {font-family: 'HelveticaNeue-Light'; font-size: 22px;}`,
      `.footer {max-width: 1180px; padding: 15px; margin: 0 auto; font-size: 14px; border-top: 1px solid #c8c8c8;}`,
      `.footer hr {display: none;}`,
      `</style>`,
      `</head>`,
      `<body>`,
      `<div class="root-container">`,
      yield render(this.url),
      `</div>`,
      `<script src="/client.js"></script>`,
      `</body>`,
      `</html>`
    )

    this.body = buf.join('')
  } catch (err) {
    console.error(err.message)
    console.trace(err)

    this.status = err.status || 400
    this.body = err.message
  }
})

export default router

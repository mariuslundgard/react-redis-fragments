import React from 'react'
import {Provider} from 'react-redux'
import {RoutingContext, match} from 'react-router'
import {createRoutes} from 'pages/routes'

function universalRouter (location, history, store) {
  return new Promise((resolve, reject) => {
    const routes = createRoutes()

    if (history) {
      store.history = history
    }

    match({routes, location}, (err, redirectLocation, renderProps) => {
      if (redirectLocation) {
        reject(new Error('Redirect not implemented'))
        // res.redirect(301, redirectLocation.pathname + redirectLocation.search)
      } else if (err) {
        err.status = 500
        reject(err)
      } else if (renderProps == null) {
        err = new Error('Not found: ' + location.pathname + location.search)
        err.status = 404
        reject(err)
      } else {
        resolve(
          <Provider store={store}>{() => (
            <RoutingContext {...renderProps}/>
          )}</Provider>
        )
      }
    })
  })
}

export default universalRouter

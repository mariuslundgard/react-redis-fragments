/* eslint react/no-danger:0 */

import React from 'react'

class Fragment extends React.Component {
  static displayName = 'Fragment'

  static propTypes = {
    name: React.PropTypes.string.isRequired
  }

  componentDidMount () {
    const scriptEl = document.createElement('script')
    scriptEl.src = `http://localhost:4000/${this.props.name}/bootstrap.js`
    document.body.appendChild(scriptEl)
  }

  render () {
    const {name, ...query} = this.props
    const queryJson = JSON.stringify(query)

    return (
      <div
        className={`fragment-container ${name}-container js-initialize`}
        dangerouslySetInnerHTML={{
          __html: `[[${name}:${queryJson}]]`
        }} />
    )
  }
}

export default Fragment

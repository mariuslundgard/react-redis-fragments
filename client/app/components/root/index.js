import React from 'react'

class Root extends React.Component {
  static displayName = 'Root'

  static propTypes = {
    children: React.PropTypes.object.isRequired
  }

  render () {
    return (
      <div className='root'>
        {this.props.children}
      </div>
    )
  }
}

export default Root

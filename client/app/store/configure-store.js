import {createStore, applyMiddleware} from 'redux'
import combinedReducers from 'reducers'

function requestMiddleware () {
  return next => action => {
    console.log('requestMiddleware')
    next(action)
  }
}

const finalCreateStore = applyMiddleware(
  requestMiddleware
)(createStore)

export default function configureStore (initialState) {
  const store = finalCreateStore(combinedReducers, initialState)
  // console.log('new store', store)
  return store
}

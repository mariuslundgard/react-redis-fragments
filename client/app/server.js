import path from 'path'
import koa from 'koa'
import serve from 'koa-static'
import router from 'router/server'
import logger from 'logger'
import fragmentRenderer from 'middleware/fragment-renderer'
import redisCache from 'middleware/redis-cache'

const app = koa()
const port = process.env.PORT || 3000

// Middleware
app.use(redisCache)
app.use(serve(path.resolve(__dirname, '../static')))
app.use(function * (next) {
  logger.info(this.method, this.url)
  yield next
})
app.use(fragmentRenderer)
app.use(router.routes())
app.use(router.allowedMethods())

// Start server
app.listen(port)
logger.info('fragment/client is listening at localhost:' + port)

var path = require('path')
var webpack = require('webpack')

module.exports = {
  entry: {
    client: './app/client.js'
  },
  output: {
    path: path.resolve(__dirname, 'static'),
    filename: '[name].js',
    library: '[name]'
  },
  module: {
    loaders: [
      { test: /\.js$/, loader: 'babel?optional[]=runtime&stage=0', exclude: /(node_modules|static|webpack)/ }
    ]
  },
  resolve: {
    root: [path.resolve(__dirname, 'app')]
  },
  plugins: [
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false
      },
      output: {
        comments: false
      }
    })
  ]
}
